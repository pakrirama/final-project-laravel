@extends('layouts.master')

@section('content')

<div class="xl">
    <div class="row justify-content-center">
        <div class="col-md-8">
             <div class="card">
                          <div class="card-body">
                              <div class="user-panel mt-3 pb-3 mb-3 d-flex">    
                                  <div class="image">
                                    <img src="{{asset('AdminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                                  </div>
                                  <div class="info">
                                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                                  </div>
                                </div>
                                <img src="https://www.mecgale.com/wp-content/uploads/2017/08/dummy-profile.png" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure." style="width:100%; height:50rem;">                              
                                   <div class="nav-icon mx-3 mt-3 row">
                                      <a href="#" class="nav-icon fa fa-heart mx-2" aria-hidden="true"></a>
                    
                                      <a href="/comment" class="nnav-icon fa fa-comment mx-2" aria-hidden="true"></a>

                                      <a href="#" class="nav-icon fa fa-paper-plane mx-2" aria-hidden="true"></a>
                                    </div>
                                    <p class='row mx-4 mt-2'><strong>{{ Auth::user()->name }} </strong></p>
                                  <p class='row mx-4'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur magnam eligendi totam culpa necessitatibus cum sapiente, fugit quam dolor inventore.</p>
                                  <hr>
                          </div>
        </div>
    </div>
</div>
@endsection 

