
@extends('layouts.master')
@section('content')

    <div class="xl">
    <div class="row justify-content-center">
        <div class="col-md-8">
            			<div class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
						<div class="banner"></div>
                        <div class="banner" style='position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 125px;
                        background-color:rgb(35, 43, 70);
                        background-position: center;
                        background-size: cover;'></div>
                        <img src="{{$profiles->foto_profile}}" alt="https://www.greenscene.co.id/wp-content/uploads/2019/10/Iron-Man-1-696x497.jpg" class="mx-auto mb-3" style="height: 150px;
                        width: 150px;
                        border-radius: 150px;
                        border: 3px solid #fff;
                        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                        z-index: 1;">
						<h3 class="mb-4">{{ Auth::user()->name }}</h3>
                        <p>Bio:</p>
                        <p>{{$profiles->bio}}</p>
                        <div>
                          <a href="/profiles/{{$profiles->user_id}}/edit"><button class="btn btn-success btn">Edit</button></a>
                        </div>
            </div>
            <div class="card">
                          <div class="card-body">
                              <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                                  
                                  <div class="image">
                                    <img src="{{$profiles->foto_profile}}" class="img-circle elevation-2" alt="User Image">
                                  </div>
                                  <div class="info">
                                    <a href="/profiles/{{ Auth::user()->id }}" class="d-block">{{ Auth::user()->name }}</a>
                                  </div>
                                </div>
                                
                               <img src="https://www.mecgale.com/wp-content/uploads/2017/08/dummy-profile.png" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure." style="width:100%; height:50rem;">                              
                                   <div class="nav-icon mx-3 mt-2 row">
                                     <i class="nav-icon fa fa-heart mx-2" aria-hidden="true"></i>
                                     
                                     <i class="nav-icon fa fa-comment mx-2" aria-hidden="true" href="/comment"></i>
                                     
                                     <i class="nav-icon fa fa-paper-plane mx-2" aria-hidden="true"></i>
                                    </div>
                                    <p class='row mx-4 justify-content-left mt-2'><strong>{{ Auth::user()->name }} </strong></p>
                                  <p class='row mx-4 justify-content-center'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur magnam eligendi totam culpa necessitatibus cum sapiente, fugit quam dolor inventore.</p>
                                  <hr>
                          </div>
                         <div class="card-body">
                              <div class="user-panel mt-3 pb-3 mb-3 d-flex">    
                                  <div class="image">
                                    <img src="{{$profiles->foto_profile}}" class="img-circle elevation-2" alt="User Image">
                                  </div>
                                  <div class="info">
                                    <a href="/profile" class="d-block">{{ Auth::user()->name }}</a>
                                  </div>
                                </div>
                                <img src="https://www.mecgale.com/wp-content/uploads/2017/08/dummy-profile.png" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure." style="width:100%; height:50rem;">                              
                                   <div class="nav-icon mx-3 mt-2 row">
                                     <i class="nav-icon fa fa-heart mx-2" aria-hidden="true"></i>
                                     
                                     <i class="nav-icon fa fa-comment mx-2" aria-hidden="true" href="/comment"></i>
                                     
                                     <i class="nav-icon fa fa-paper-plane mx-2" aria-hidden="true"></i>
                                    </div>
                                    <p class='row justify-content-left mx-4 mt-2'><strong>{{ Auth::user()->name }} </strong></p>
                                  <p class='row justify-content-center mx-4'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur magnam eligendi totam culpa necessitatibus cum sapiente, fugit quam dolor inventore.</p>
                                  <hr>
                          </div>
                        </div>
                  </div>
              </div>

</div>
        @endsection