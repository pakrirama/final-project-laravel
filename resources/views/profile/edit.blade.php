
@extends('layouts.master')
@section('content')
<div class="xl">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
                <div class="banner"></div>
                    <div class="banner" style='position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 125px;
                        background-color:rgb(35, 43, 70);
                        background-position: center;
                        background-size: cover;'></div>
                        <img src="{{$profiles->foto_profile}}" alt="foto" class="mx-auto mb-3" style="height: 150px;
                        width: 150px;
                        border-radius: 150px;
                        border: 3px solid #fff;
                        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                        z-index: 1;">
						<h3 class="mb-4">{{ Auth::user()->name }}</h3>
                        <div style="height:55vh;">
                            <form method='POST' action='/profiles/{{$profiles->id}}' > 
                            @csrf
                            @method('put')
                            <div class="form-group" style="margin-top:5rem">
                                <label for="exampleInputEmail1">Image URl</label>
                                <input type="text" class="form-control" name="foto_profile" placeholder="image url" value="{{$profiles->foto_profile}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Bio</label>
                                <input type="text" class="form-control" name="bio" placeholder="Bio" value="{{$profiles->bio}}">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                        <br>
                        <a href="/profiles/{{ Auth::user()->id }}"><button type="submit" class="btn btn-danger">Cancle</button></a>
                        </div>
                        
                        <div>
                        </div>
            </div>
        </div>
    </div>
</div>
@endsection

