@extends('layouts.master')
@section('judul')
Halaman post
@endsection
@section('content')
<div class="container" style="padding-top:5rem;">

    <form action="/post" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group container" >
            <label>caption</label>
            <input type="text" name="caption" class="form-control">
            @error('caption')
            <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="text" name="image" class="form-control" >
                <input type="number" name="user_id" hidden val="{{ Auth::user()->id }}" >
            </div>
            @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label>tag</label>
                <select name="tag" class="form-control" >
                    <option value="">--- pilih tag ---</option>
                    @foreach ($tag as $item)
                    <option name="tag_id" value="{{$item->id}}">{{$item->tag}}</option>
                    @endforeach
                </select>
            </div>
            @error('tag')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <button type="submit" class="btn btn-primary">post</button>
        </form>
    </div>

@endsection
