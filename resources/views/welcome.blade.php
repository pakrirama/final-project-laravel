<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
             <!-- Bootstrap core CSS -->
    <link href="{{asset('grad-school-1.0.0/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{asset('grad-school-1.0.0/assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('grad-school-1.0.0/assets/css/templatemo-grad-school.css')}}">
    <link rel="stylesheet" href="{{asset('grad-school-1.0.0/assets/css/owl.css')}}">
    <link rel="stylesheet" href="{{asset('grad-school-1.0.0/assets/css/lightbox.css')}}">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
             
        <!-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div> -->

   <!-- header-->
        <header class="main-header clearfix" role="header">
        
            <div class="logo">
            <a href="#"><em>Social</em> Media</a>
            </div>
            <a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a>
            <nav id="menu" class="main-nav" role="navigation">
            <ul class="main-menu">
            
                <li><a href="#section2">About Us</a></li>
               
            </nav>
        </header>

        <!--HOME-->
        <!-- ***** Main Banner Area Start ***** -->
        <section class="section main-banner" id="top" data-section="section1">
      <video autoplay muted loop id="bg-video">
          <source src="{{asset('grad-school-1.0.0/assets/images/course-video.mp4')}}" type="video/mp4" />
      </video>

      <div class="video-overlay header-text">
          <div class="caption">
              <h2><em>SOCIAL</em> MEDIA</h2>
              <h6>is a place for you to express yourself</h6>
              <div class="main-button"><br>
              @if (Route::has('login'))
                    @auth
                        <div><a href="{{ url('/home') }}">Home</a></div><br>
                        <div><a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                       Logout 
                                    </a></div><br>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    @else
                        <div ><a href="/login">Login</a></div><br>
                        @if (Route::has('register'))
                        <div><a href="{{ route('register') }}">Register Now!</a></div><br>
                @endif
                    @endauth 
            @endif
              </div>
          </div>
      </div>
  </section>
  <!-- ***** Main Banner Area End ***** -->

  <!--ABOUT US-->
  <section class="section why-us" data-section="section2">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading">
            <h2>About US</h2>
          </div>
        </div>
        <div class="col-md-12">
          <div id='tabs'>
            <ul>
              <li><a href='#tabs-1'>Kelompok 11</a></li>
              <li><a href='#tabs-2'>Sanbercode</a></li>
              <li><a href='#tabs-3'>Laravel</a></li>
            </ul>
            <section class='tabs-content'>
              <article id='tabs-1'>
                <div class="row">
                  <div class="col-md-6">
                    <img src="{{asset('grad-school-1.0.0/assets/images/choose-us-image-01.png')}}" alt="">
                  </div>
                  <div class="col-md-6">
                    <h4>Kelompok 11</h4>
                    <p>Kelompok 11 memiliki tiga anggota, yaitu:
                      <br>M.Fakhri R.
                      <br>Rizka Savitri K.D.
                      <br>Wahab Abdullah
                      </ul>
                    </p>
                  </div>
                </div>
              </article>
              <article id='tabs-2'>
                <div class="row">
                  <div class="col-md-6">
                    <img src="{{asset('grad-school-1.0.0/assets/images/choose-us-image-02.png')}}" alt="">
                  </div>
                  <div class="col-md-6">
                    <h4>Sanbercode</h4>
                    <p>Social Media Developer Santai Berkualitas</p> 
                    <p>Benefit Join di Sanbercode:
                      <br>Mendapatkan motivasi dari sesama developer
                      <br>Sharing knowledge dari para mastah Sanber
                      <br>Dibuat oleh calon web developer terbaik
                      </p>
                  </div>
                </div>
              </article>
              <article id='tabs-3'>
                <div class="row">
                  <div class="col-md-6">
                    <img src="{{asset('grad-school-1.0.0/assets/images/choose-us-image-03.png')}}" alt="">
                  </div>
                  <div class="col-md-6">
                    <h4>Laravel</h4>
                    <p> Laravel adalah kerangka kerja aplikasi web berbasis PHP yang sumber terbuka, menggunakan konsep Model-View-Controller. Laravel berada dibawah lisensi MIT, dengan menggunakan GitHub sebagai tempat berbagi kode.</p>
                  </div>
                </div>
              </article>
            </section>
          </div>
        </div>
      </div>
    </div>
  </section>
               
         
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p><i class="fa fa-copyright"></i> Copyright 2020 by Grad School  
          
           | Design: <a href="" rel="sponsored" target="_parent">TemplateMo</a><br>
           Distributed By: <a href="" rel="sponsored" target="_blank">ThemeWagon</a><br>
           Modified By: <a href=""> kelompok 11 </a>
          
          </p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="{{asset('grad-school-1.0.0/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{asset('grad-school-1.0.0/assets/js/isotope.min.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/assets/js/owl-carousel.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/assets/js/lightbox.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/assets/js/tabs.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/assets/js/video.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/assets/js/slick-slider.js')}}"></script>
    <script src="{{asset('grad-school-1.0.0/assets/js/custom.js')}}"></script>
    <script>
        //according to loftblog tut
        $('.nav li:first').addClass('active');

        var showSection = function showSection(section, isAnimate) {
          var
          direction = section.replace(/#/, ''),
          reqSection = $('.section').filter('[data-section="' + direction + '"]'),
          reqSectionPos = reqSection.offset().top - 0;

          if (isAnimate) {
            $('body, html').animate({
              scrollTop: reqSectionPos },
            800);
          } else {
            $('body, html').scrollTop(reqSectionPos);
          }

        };

        var checkSection = function checkSection() {
          $('.section').each(function () {
            var
            $this = $(this),
            topEdge = $this.offset().top - 80,
            bottomEdge = topEdge + $this.height(),
            wScroll = $(window).scrollTop();
            if (topEdge < wScroll && bottomEdge > wScroll) {
              var
              currentId = $this.data('section'),
              reqLink = $('a').filter('[href*=\\#' + currentId + ']');
              reqLink.closest('li').addClass('active').
              siblings().removeClass('active');
            }
          });
        };

        $('.main-menu, .scroll-to-section').on('click', 'a', function (e) {
          if($(e.target).hasClass('external')) {
            return;
          }
          e.preventDefault();
          $('#menu').removeClass('active');
          showSection($(this).attr('href'), true);
        });

        $(window).scroll(function () {
          checkSection();
        });
    </script>
    </body>
</html>
