 @extends('layouts.master')
@section('tag')
Halaman tag
@endsection
@section('content')
<table class="table">
    <center><h1>DATA TAG</h1></center>
    <a href="/tag/create" class="btn btn-primary mb-3"> Tambah Tags </a>
    <thead class="thead-dark">
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Tag</th>
        <th scope="col">Action</th>

      </tr>
    </thead>
    <tbody>
             @forelse ($tags as $key => $item)
                <tr>
                    <th>{{$key+1}}</th>
                    <td>{{$item->tag}}</td>
                    <td>
                        <form action="/tag/{{$item->id}}" method="post">
                         @csrf
                         @method('delete')
                        <input type="submit" class="btn btn-warning btn-sm" value="delete">
                        </form>
                    </td>
                </tr>

             @empty
               <tr>
                    <td> Data Tag Masih Kosong </td>
               </tr>
             @endforelse

    </tbody>
  </table>

  @endsection
