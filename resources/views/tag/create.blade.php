 @extends('layouts.master')
@section('tag')
Halaman tag
@endsection
@section('content')
        <form action="/tag" method="POST">
            @csrf
            <center> <h1> Add Tags </h1></center>
            <div class="form-group">
                <label>Tags</label>
                <input type="text" name="tags" class="form-control">
                @error('tags')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <br>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>


@endsection
