@extends('layouts.master')

@section('content')

<div class="container-xl-center">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               
                <!-- CEK LAGI -->
                <div class="card-body">
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                          <img src="{{asset('AdminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                          <a href="/profile" class="d-block">{{ Auth::user()->name }}</a>
                        </div>
                      </div>

                    <div class="text-center">
                        <figure class="figure">
                          <img src="https://www.mecgale.com/wp-content/uploads/2017/08/dummy-profile.png" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
                          <figcaption class="figure-caption text-center">A caption for the above image.</figcaption>
                        </figure>
                        </div>
                        <label>Comment</label>
                        <input type="comment" class="form-control" id="comment" placeholder="Enter your comment here">                        
                    
                    <button type="button" class="btn btn-outline-info"> Send</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
