<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

route::resource('post', 'PostController');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
 //CRUD Casts
   
Route::resource('posts', 'PostController');
Route::resource('profiles', 'ProfileController');


Route::get('/master', function(){
    return view('layouts.master');
});

// Route::get('/profile', function(){
//     return view('profile.show');
// });
Route::get('/profile/edit', function(){
    return view('profile.edit');
});




//COMMENT
Route::get('/comment', function(){
    return view('comment.comment');
});

//CREATE

//route untuk form buat data tag
Route::get('/tag/create', 'TagController@create');

Route::post('/tag', 'TagController@store');


//READ

//route untuk tampil
route::get('/tag', 'TagController@index');



//DELETE
route::delete('/tag/{tags_id}', 'TagController@destroy');

//CRUD Post


});



