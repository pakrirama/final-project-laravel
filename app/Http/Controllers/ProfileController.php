<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profiles = DB::table('profiles')->where('user_id',$id)->first();
        
        return view('profile.show',compact('profiles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $profiles = DB::table('profiles')->where('user_id',$id)->first();
          
          
        
        return view('profile.edit',compact('profiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('profiles')->where('user_id',$id)->update([
            'foto_profile'=>$request['foto_profile'],
            'bio'=>$request['bio']
        ]);
        
        
        Alert::success('Congrats', 'Your Profile Updated');
        return view('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

// <?php

// namespace App\Http\Controllers;

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
// use App\Film; // untuk Eloquent
// use App\Cast; // untuk Eloquent

// class FilmController extends Controller
// {

//     public function __construct()
//     {
//         $this->middleware('auth')->except('index','show'); //memerlukan auth kecuali index dan show
//     }

//      public function create(){
//         return view('film.create');
//     }

//     public function store(Request $request){
//         // dd($request->all());
//         $request->validate([
//             'judul' => 'required|min:5',
//             'ringkasan' => 'required',
//             'tahun' => 'required',
//             'poster' => 'required',
//             'genre_id' => 'required|numeric|lt:5',
//         ],
        // [
        //     'judul.required' => 'Judul harus diisi!',
        //     'judul.min' => 'Judul harus lebih dari 5 karakter!',
        //     'ringkasan.required' => 'Ringkasan harus diisi!',
        //     'tahun.required' => 'Tahun harus diisi!',
        //     'poster.required' => 'Poster harus diisi!',
        //     // 'genre_id.required' => 'Genre harus diisi!',
        // ]
        // );

        //mengunakan metode Save()
        // $film = new Film;
        // $film->judul = $request->judul;
        // $film->ringkasan = $request->ringkasan;
        // $film->tahun = $request->tahun;
        // $film->poster = $request->poster;
        // $film->genre_id = $request->genre_id;
        // $film->save();

//         //menggunakan metode mass Assignment
//         $film = Film :: create([
//             'judul' => $request['judul'],
//             'poster' => $request['poster'],
//             'tahun' => $request['tahun'],
//             'genre_id' => $request['genre_id'],
//             'ringkasan' => $request['ringkasan'],
//         ]); 
//         return redirect('/film');
//     }
    
//     public function index(){
//         // $film = DB::table('film')->get(); 
//         $film = Film::all();
//         return view('film.index',compact('film'));
//     }

//     public function show($id){
//         // $film = DB::table('film')->where('id',$id)->first();
//         $film = Film::find($id);
        
//         return view('film.show',compact('film'));

//     }
    
//     public function edit($id){
//         $film = DB::table('film')->where('id',$id)->first();
        
//         return view('film.edit',compact('film'));

//     }
    
//     public function update($id,Request $request){
//         // DB::table('film')->where('id',$id)->update([
//         //     'judul'=>$request['judul'],
//         //     'poster'=>$request['poster'],
//         //     'tahun'=>$request['tahun'],
//         //     'genre_id'=>$request['genre_id'],
//         //     'ringkasan'=>$request['ringkasan'],
//         // ]);

//         $update = Film::where('id',$id)->update([
//             'judul'=>$request['judul'],
//             'poster'=>$request['poster'],
//             'tahun'=>$request['tahun'],
//             'genre_id'=>$request['genre_id'],
//             'ringkasan'=>$request['ringkasan'],
//         ]);
//         return redirect('/film');

//     }

    
//     public function destroy($id){
//         // DB::table('film')->where('id',$id)->delete();
//         Film::destroy($id);
        
//         return redirect('/film');

//     }
// }
